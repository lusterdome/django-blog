from django.conf.urls import patterns, include, url
from django.conf import settings

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'blog.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^login/', 'django.contrib.auth.views.login'),
    url(r'^logout/', 'django.contrib.auth.views.logout', {'next_page': '/'}),
    url(r'^signup/', 'blogapp.views.signup'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'blogapp.views.index'),
    url(r'^blog/(?P<postid>\d+)', 'blogapp.views.postbyid'),
    url(r'^like/(?P<posttype>\w+)/(?P<postid>\d+)', 'blogapp.views.likecount'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
            (r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
            )
