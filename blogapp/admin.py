from django.contrib import admin
from models import Post, Comment

class PostAdmin(admin.ModelAdmin):

    list_display = (
            'user',
            'date',
            'title',
            'image',
            'likes_',
            )

    def likes_(self, obj):
        return obj.likes.count()

class CommentAdmin(admin.ModelAdmin):

    list_display = (
            'user',
            'date',
            'post',
            'likes_',
            )

    def likes_(self, obj):
        return obj.likes.count()

admin.site.register(Post, PostAdmin)
admin.site.register(Comment, CommentAdmin)
