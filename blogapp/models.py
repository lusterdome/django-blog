from django.db import models
from django.contrib.auth.models import User

class Post(models.Model):

    user = models.ForeignKey(User, related_name='post_author')
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=256)
    image = models.ImageField(upload_to='static/posts')
    likes = models.ManyToManyField(User, related_name='post_likes')

    def __str__(self):
        return '{} - {} (on {})'.format(self.user, self.title, self.date)

class Comment(models.Model):

    user = models.ForeignKey(User, related_name='comment_author')
    post = models.ForeignKey(Post)
    text = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    likes = models.ManyToManyField(User, related_name='comment_likes')

