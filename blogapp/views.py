from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm

from .models import Post, Comment
from .forms import NewUserCreationForm

def index(request):

    posts = Post.objects.all().order_by('-date')

    # if request.user.is_authenticated():
    #     current_user = request.user
    #     for p in posts:
    #         if p.likes.filter(username=current_user.username).exists():
    #             # unlike
    #             p.toggle = -1
    #         else:
    #             # like
    #             p.toggle = 1

    context = {}
    context['posts'] = posts

    return render(request, 'base.html', context)

def postbyid(request, postid):

    post = Post.objects.get(id=postid)
    comments = Comment.objects.all().filter(post=post)

    sort_param = request.GET.get('sort', None)
    if sort_param == 'new':
        comments.order_by('-date')
    elif sort_param == 'old':
        comments.order_by('date')
    elif sort_param == 'best':
        comments.order_by('-likes')
    elif sort_param == 'worst':
        comments.order_by('likes')
    else:
        comments.order_by()

    context = {}
    context['post'] = post
    context['comments'] = comments

    return render(request, 'individual.html', context)

@login_required(login_url='/login/', redirect_field_name='/')
def likecount(request, posttype, postid):
    current_user = request.user

    if posttype == 'post':
        post = Post.objects.get(id=postid)
    elif posttype == 'comment':
        post = Comment.objects.get(id=postid)
    else:
        return HttpResponse("Error")

    if post.likes.filter(username=current_user.username).exists():
        # unlike
        post.likes.remove(current_user)
    else:
        # like
        post.likes.add(current_user)

    post.save()
    return HttpResponse(post.likes.count())

def signup(request):
    form = NewUserCreationForm
    if request.method == 'POST':
        form = NewUserCreationForm(request.POST)
        if form.is_valid():
            user = form.save()
            user = authenticate(
                    username=request.POST['username'],
                    password=request.POST['password1'])
            login(request, user)
            return redirect('/')

    context = {}
    context['form'] = form

    return render(request, 'registration/signup.html', context)
